package nl.utwente.di.temperature;

public class Quoter {
    public static double getFahrenheit (String celsius) {
       //(0°C × 9/5) + 32 = 32°F
        if (celsius.isBlank()) {
            return 0.0;
        }
        else {
            double conv = Double.parseDouble(celsius);
            return conv * (9 / 5) + 32;
        }
    }

    public static void main(String[] args) {
        System.out.println(getFahrenheit(""));
    }
}
